# Copyright (C) 2018  Khadija Tariq, Amin Bandali

# This file is part of ic3z3.

# ic3z3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <https://www.gnu.org/licenses/>.


from pysmt.shortcuts import (FreshSymbol, TRUE, And, binary_interpolant,
                             Solver, Symbol, get_type,
                             Or, Not, Iff)
from pysmt.typing import BOOL
import z3


class refiner:
    def __init__(self, ts, logger):
        self._ts = ts
        self._preds = TRUE()
        self._logger = logger
        self._converter = Solver("z3").converter

    def add_predicate(self, pred):
        self._preds = And(self._preds, pred)

    def refine(self, cex):
        self._assertions = []
        for k in range(len(cex)):
            clause = self.get_and(cex[k])
            assertion = self.unroll(clause, k)
            if k != len(cex) - 1:
                assertion = And(assertion, self.unroll(self._ts["trans"], k))
            self._assertions.append(assertion)
            self._logger.debug("abstract state {} : {}".format(k, clause))

        self._solver = z3.Solver()
        for assertion in self._assertions:
            self._solver.add(self._converter.convert(assertion))
        result = self._solver.check()

        return result == z3.unsat

    def get_predicates(self, cex):
        self._logger.debug("counterexample was spurious, extracting interpolants")
        preds = self.get_interpolants(self._assertions)
        preds = self.minimize_predicates(cex, preds)
        return preds

    def get_and(self, clauses):
        if len(clauses) == 0:
            return TRUE()
        s = clauses[0]
        for c in clauses[1:]:
            s = And(s, c)
        return s

    def unroll(self, clause, idx):
        subst = {}
        for var in list(clause.get_free_variables()):
            if var in self._ts["nextVar"]:
                subst[var] = Symbol(self._ts["cur"][var].symbol_name()
                                    + '@' + str(idx + 1), get_type(var))
            else:
                subst[var] = Symbol(var.symbol_name() + '@' + str(idx),
                                    get_type(var))
        return clause.substitute(subst)

    def unroll_back(self, clause):
        subst = {}
        for var in list(clause.get_free_variables()):
            subst[var] = Symbol(var.symbol_name().split("@")[0], get_type(var))
        return clause.substitute(subst)

    def get_interpolants(self, assertions):
        preds = []
        for idx in range(len(assertions) - 1):
            A = self.get_and(assertions[:idx + 1])
            B = self.get_and(assertions[idx + 1:])
            itp = binary_interpolant(A, B, solver_name="msat")
            self._logger.debug("got interpolant {} : {}".format(idx, itp))
            preds = preds + list(self.unroll_back(itp).get_atoms())
        return preds

    def minimize_predicates(self, cex, preds):
        solver = z3.Solver()
        solver.set(unsat_core=True)
        absTrans = self.abstract(self._ts["trans"])
        for k in range(len(cex)):
            clause = self.get_and(cex[k])
            solver.add(self._converter.convert(self.unroll(clause, k)))
            if k != len(cex) - 1:
                solver.add(self._converter.convert(self.unroll(absTrans, k)))
                solver.add(self._converter.convert(self.unroll(self._preds, k)))
        labels = []
        for pred in preds:
            nextPred = self.next(pred)
            absPred = self.abstract(nextPred)
            nextToAbs = Iff(nextPred, absPred)
            label = z3.FreshBool()
            for k in range(len(cex) - 1):
                solver.add(z3.Or(z3.Not(label),
                                        self._converter.convert(self.unroll(nextToAbs, k - 1))))
            labels.append(label)
        assert(solver.check(labels) == z3.unsat)
        core = solver.unsat_core()
        newPreds = []
        for idx in range(len(preds)):
            if labels[idx] in core:
                newPreds.append(preds[idx])
        return newPreds

    def get_counterexample(self, abs_cex):
        cex = []
        n = len(abs_cex)
        model = self._solver.model()
        for i in range(n):
            clauses = []
            for var in self._ts["stateVar"]:
                z3var = self._converter.convert(var)
                value = model[self._converter.convert(self.unroll(var, i))]
                clauses.append(self.getVar(var, z3var, value))
            if i != n - 1:
                for var in self._ts["inputVar"]:
                    value = model[self._converter.convert(self.unroll(var, i))]
                    clauses.append(self.getVar(var, z3var, value))
            cex.append(clauses)
        return cex

    def getVar(self, var, var1, var2):
        if var.is_symbol(BOOL):
            if var2:
                return var1
            return z3.Not(var1)
        return (var2 == var1)

    def abstract(self, formula):
        # substitue all next variables with their abstract version in formula
        subst = {}
        for var in self._ts["nextVar"]:
            if not var.is_symbol(BOOL):
                cur = self._ts["cur"][var]
                abstractSymbol = Symbol(cur.symbol_name() + "^", get_type(var))
                subst[var] = abstractSymbol
        return formula.substitute(subst)

    def next(self, formula):
        # substitute all variables with their next state in formula
        return formula.substitute(self._ts["next"])
