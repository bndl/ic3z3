# Copyright (C) 2018  Khadija Tariq, Amin Bandali

# This file is part of ic3z3.

# ic3z3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <https://www.gnu.org/licenses/>.

import warnings

with warnings.catch_warnings():
    warnings.filterwarnings("ignore",category=DeprecationWarning)
    import sys
    from pysmt.smtlib.parser import SmtLibParser
    from pysmt.shortcuts import get_type, Symbol
    from ic3 import ic3


def read_ts(filename):
    # check if file with filename exists
    try:
        f = open(filename, 'r')
        f.close()
    except FileNotFoundError:
        print("File with name " + filename + " does not exist!")
        sys.exit()

    transition_system = {}

    # get annotations from file
    parser = SmtLibParser()
    script = parser.get_script_fname(filename)
    ann = script.annotations

    # store init, transition relation and invariant property
    transition_system["init"] = list(ann.all_annotated_formulae("init"))[0]
    transition_system["trans"] = list(ann.all_annotated_formulae("trans"))[0]
    transition_system["prop"] = list(
        ann.all_annotated_formulae("invar-property"))[0]

    # store all state variables and their next states
    transition_system["stateVar"] = list(ann.all_annotated_formulae("next"))
    transition_system["nextVar"] = []
    transition_system["cur"] = {}
    transition_system["next"] = {}
    for var in transition_system["stateVar"]:
        nextVar = list(ann[var]["next"])[0]
        if nextVar[0] == "|":
            nextVar = nextVar[1:-1]
        nextVarSymbol = Symbol(nextVar, get_type(var))
        transition_system["nextVar"].append(nextVarSymbol)
        transition_system["cur"][nextVarSymbol] = var
        transition_system["next"][var] = nextVarSymbol

    # add input variables: variable in :trans but with no next state
    transition_system["inputVar"] = []
    for var in transition_system["trans"].get_free_variables():
        if var not in transition_system["stateVar"] and var not in transition_system["nextVar"]:
            transition_system["inputVar"].append(var)
            transition_system["cur"][var] = var
            transition_system["next"][var] = var

    return transition_system


def get_options(args):
    options = {
        'verbosity': 0,
        'witness': False,
        'filename': ''
    }
    idx = 0
    while idx < len(args):
        arg = args[idx]
        if arg == "-v" or arg == "--verbose":
            try:
                options['verbosity'] = int(args[idx+1])
            except ValueError:
                print("Error: invalid verbosity level (must be integer)!")
                sys.exit()
            idx += 1
        elif arg == "-w" or arg == "--witness":
            options['witness'] = True
        elif arg == "-h" or arg == "--help":
            print("USAGE: main.py [OPTIONS] INPUTFILENAME")
            print("NOTE: input file must be in vmt format")
            print("OPTIONS: \t-v N or --verbosity N: set verbosity level")
            print('\t\t-w or --witness: print counterexample/invariant')
            sys.exit()
        elif "." in arg:
            options['filename'] = arg
        else:
            print("Error: Invalid command-line option (try -h or --help)!")
            sys.exit()
        idx += 1
    if options['filename'] == '':
        print("Error: You need to give a filename as input!")
        sys.exit()
    elif options['filename'][-4:] != ".vmt":
        print("Error: The input file must be in VMT format!")
        sys.exit()
    return options


def main():
    options = get_options(sys.argv[1:])

    transition_system = read_ts(options['filename'])
    solver = ic3(transition_system, options)
    result = solver.check_safety()
    solver.print_stats()
    print("Result: " + result)


if __name__ == "__main__":
    main()
