# Copyright (C) 2018  Khadija Tariq, Amin Bandali

# This file is part of ic3z3.

# ic3z3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <https://www.gnu.org/licenses/>.


import logging
import verboselogs
import time
from pysmt.shortcuts import (Solver, Symbol, FreshSymbol, FALSE,
                             get_type, Iff, Not, Implies, And, Or)
import z3
from pysmt.typing import BOOL
import queue as Q
from refiner import refiner


class ic3:
    def __init__(self, ts, options):
        self._ts = ts
        self._options = options
        self._logger = verboselogs.VerboseLogger('ic3z3')
        self._logger.addHandler(logging.StreamHandler())
        self._logger.setLevel(self.get_logger_verbosity(options['verbosity']))
        self._init = z3.Bool("init")
        self._trans = z3.Bool("trans")
        self._bad = z3.Bool("bad")
        self._solver = z3.Solver()
        self._solver.set(unsat_core=True)
        self._converter = Solver(name="z3").converter
        self._refiner = refiner(ts, self._logger)
        self._preds = []
        self._stateVars = []
        self._predLabels = {}
        self._labelPreds = {}
        self._nextLabels = {}
        self._frames = []
        self._frameLabels = []
        self._stats = {
            'num_refinements': 0,
            'solve_calls': 0,
            'added_cubes': 0,
            'subsumed_cubes': 0,
            'solve_time': 0,
            'rec_block_time': 0,
            'propagate_time': 0,
            'refinement_time': 0
        }


    # main function: check safety of program
    def check_safety(self):
        start_time = time.time()
        self.initialize()

        # check for 0-step counterexamples
        status = self.check_init_cases()
        if status == z3.sat:
            if self._options['witness']:
                cex = self.get_model()
                self._cex = self.concretize(cex)
                self.print_initial_counterexample()
            self._stats["total_time"] = time.time() - start_time
            return "Unsafe"

        while True:
            # BLOCKING PHASE
            # Check if current frame intersects with bad states
            while self.check_bad_state() == z3.sat:
                # Get bad state and recursively block it in frames
                bad = self.get_bad_state()
                result = self.rec_block(bad)
                # check if counterexample was found
                if not result:
                    self._logger.notice("found counterexample at depth {}".format(self.get_depth()))
                    self._stats["total_time"] = time.time() - start_time
                    self.print_counterexample()
                    return "Unsafe"

            # PROPAGATION PHASE
            # extend frame sequence by one
            self.add_frame()
            result = self.propagate()
            # check if invariant was found
            if result:
                self._stats["total_time"] = time.time() - start_time
                return "Safe"

    def initialize(self):
        for var in self._ts["stateVar"]:
            if var.is_symbol(BOOL):
                self._stateVars.append(var)
                self._labelPreds[var] = var
                self._nextLabels[var] = self.next(var)

        # add init to solver with init label
        self._solver.add(z3.Implies(self._init, self._converter.convert(self._ts["init"])))

        # add abstraction of transition relation with trans label
        self._solver.add(z3.Implies(self._trans, self._converter.convert(self.abstract(self._ts["trans"]))))

        # get predicates from init and invariant property
        preds = list(self._ts["init"].get_atoms()) + list(self._ts["prop"].get_atoms())
        for p in preds:
            if not p.is_symbol(BOOL):
                self.add_predicate(p)

        # add bad to solver with bad label
        self._solver.add(z3.Implies(self._bad, self._converter.convert(Not(self._ts["prop"]))))

        # initalize first frame
        self._frames.append([])
        self._frameLabels.append(self._init)
        self._logger.warning("Initialized IC3 with {} state variables, {} input variables and {} predicates!"
            .format(len(self._ts["stateVar"]), len(self._ts["inputVar"]), len(self._preds)))

    def check_init_cases(self):
        # check for intersection between initial state and bad
        return self.solve([self._frameLabels[0], self._bad, z3.Not(self._trans)])

    def activate_frame(self, idx):
        # deactivates all frames upto idx and activates frames from idx to depth
        return [z3.Not(frame) for frame in self._frameLabels[:idx]] + [frame for frame in self._frameLabels[idx:]]

    def abstract(self, formula):
        # substitue all next variables with their abstract version in formula
        subst = {}
        for var in self._ts["nextVar"]:
            if not var.is_symbol(BOOL):
                cur = self._ts["cur"][var]
                abstractSymbol = Symbol(cur.symbol_name()+"^", get_type(var))
                subst[var] = abstractSymbol
        return formula.substitute(subst)

    def next(self, formula):
        # substitute all variables with their next state in formula
        return formula.substitute(self._ts["next"])

    def add_predicate(self, p):
        # link next state of predicate with its abstract state
        self._preds.append(p)
        nextPred = self.next(p)
        abstractPred = self.abstract(nextPred)
        self._solver.add(z3.Implies(self._trans, self._converter.convert(Iff(nextPred, abstractPred))))

        # add predicate and its next state with new labels
        p_state = FreshSymbol()
        p_next = FreshSymbol()
        self._predLabels[p] = p_state
        self._labelPreds[p_state] = p
        self._nextLabels[p_state] = p_next
        self._solver.add(self._converter.convert(And(Iff(p_state, p), Iff(p_next, nextPred))))
        self._stateVars.append(p_state)
        self._logger.verbose("adding predicate {}: {} := {}".format(len(self._preds), p_state, p))

        # add predicate in refiner
        self._refiner.add_predicate(Iff(nextPred, abstractPred))

    def print_initial_counterexample(self):
        # print counterexample by concretizing smt model
        print("Counterexample:")
        print("( and")
        for clause in self._cex:
            print("  ", end=" ")
            print(clause)
        print(")")

    def print_counterexample(self):
        # print counterexample by concretizing smt model
        print("Counterexample:")
        for idx in range(len(self._cex)):
            print("Step {}".format(idx))
            print("( and")
            for clause in self._cex[idx]:
                print("  ", end=" ")
                print(clause)
            print(")")

    def check_bad_state(self):
        # check interesection between current frame and bad state
        k = self.get_depth()
        return self.solve(self.activate_frame(k)+[self._bad, z3.Not(self._trans)])

    def get_bad_state(self):
        # get bad state from smt model and generalize it
        bad = self.get_model()
        bad = self.generalize_bad(bad)
        self._logger.debug("got bad cube of size {}".format(len(bad)))
        self._logger.spam(self.concretize(bad))
        return bad

    def get_depth(self):
        return len(self._frames)-1

    def get_model(self):
        cex = []
        # get state of all predicates based on solver model
        for var in self._stateVars:
            pred = var
            if not self._solver.model()[self._converter.convert(var)]:
                pred = Not(var)
            cex.append(pred)
        return cex

    def concretize(self, preds):
        # concretize set of predicates (replace label with predicate)
        result = []
        for p in preds:
            if p in self._labelPreds:
                result.append(self._labelPreds[p])
            elif Not(p) in self._labelPreds:
                result.append(Not(self._labelPreds[Not(p)]))
        return result

    def generalize_bad(self, bad):
        # generalize bad using unsat core
        gen_bad = []
        self._solver.push()
        self._solver.add(self._converter.convert(self._ts["prop"]))
        zbad = [self._converter.convert(b) for b in bad]
        result = self.solve(zbad)
        assert(result == z3.unsat)
        core = self._solver.unsat_core()
        for b in bad:
            if self._converter.convert(b) in core:
                gen_bad.append(b)
        self._solver.pop()
        return gen_bad

    def rec_block(self, bad):
        # strengthen frames so bad is blocked in current frame or return counterexample
        start_time = time.time()
        proof_queue = Q.PriorityQueue()
        k = self.get_depth()
        counter = 0
        proof_queue.put((k, counter, bad, None))
        while not proof_queue.empty():
            counter += 1
            proof_obligation = proof_queue.queue[0]
            self._logger.debug("checking proof obligation of size {} at index {}".format(len(proof_obligation[2]), proof_obligation[0]))
            self._logger.spam(self.concretize(proof_obligation[2]))
            if proof_obligation[0] == 0:
                cex = []
                while proof_obligation:
                    cex.append(proof_obligation[2])
                    proof_obligation = proof_obligation[3]
                result = self.refine_abstraction(cex)
                self._stats['rec_block_time'] += time.time() - start_time
                return result
            if self.is_blocked(proof_obligation):
                proof_queue.get()
            else:
                if self.check_rel_ind(proof_obligation[0], proof_obligation[2]):
                    candidate = self.get_candidate(proof_obligation[2])
                    self._logger.debug("CTI blocked cube of size {} inductive relative to frame {}".format(len(candidate), proof_obligation[0]-1))
                    self._logger.spam(self.concretize(candidate))
                    candidate = self.generalize(candidate, proof_obligation[0])
                    candidate, idx = self.push(candidate, proof_obligation[0])
                    self.add_blocked(candidate, idx)
                    if idx < k:
                        proof_queue.put((proof_obligation[0]+1, counter, proof_obligation[2], proof_obligation[3]))
                    proof_queue.get()
                else:
                    cti = self.get_model()
                    self._solver.pop()
                    self._logger.debug("CTI found of size {}".format(len(cti)))
                    self._logger.spam(self.concretize(cti))
                    proof_queue.put((proof_obligation[0]-1, counter, cti, proof_obligation))
        self._stats['rec_block_time'] += time.time() - start_time
        return True

    def is_initial(self, bad):
        # check if bad state intersects with initial
        bad = [self._converter.convert(b) for b in bad]
        return self.solve(bad+[self._init, z3.Not(self._trans), z3.Not(self._bad)])

    def is_blocked(self, proof_obligation):
        # check if there is intersection between proof_obligation[2] and frames[proof_obligation[0]]
        for i in range(proof_obligation[0], len(self._frames)):
            for j in range(len(self._frames[i])):
                if self.subsumes(self._frames[i][j], proof_obligation[2]):
                    self._stats['subsumed_cubes'] += 1
                    return True

        if len(self._preds) == 0:
            return False

        bad = [self._converter.convert(clause) for clause in proof_obligation[2]]
        result = self.solve(self.activate_frame(proof_obligation[0])+[z3.Not(self._trans), z3.Not(self._bad)]+bad)
        return result == z3.unsat

    def check_rel_ind(self, idx, cube):
        # check if !proof_obligation[1] is inductive relative to frame[proof_obligation[0]-1]
        self._solver.push()
        c = FALSE()
        for clause in cube:
            c = Or(c, Not(clause))
        self._solver.add(self._converter.convert(c))
        assumptions = [self._converter.convert(clause.substitute(self._nextLabels)) for clause in cube]
        assumptions = assumptions + self.activate_frame(idx-1) + [self._trans, z3.Not(self._bad)]
        return self.solve(assumptions) == z3.unsat

    def get_candidate(self, clauses):
        # generalize clauses to get a stronger candidate
        primes = [clause.substitute(self._nextLabels) for clause in clauses]
        core = self._solver.unsat_core()
        candidate = []
        rest = []
        for idx in range(len(primes)):
            prime = primes[idx]
            if self._converter.convert(prime) in core:
                candidate.append(clauses[idx])
            else:
                rest.append(clauses[idx])
        self._solver.pop()

        # make sure init & candidate is not sat
        if self.is_initial(candidate) == z3.sat:
            assert(self.is_initial(candidate+rest) == z3.unsat)
            core = self._solver.unsat_core()
            for r in rest:
                if self._converter.convert(r) in core:
                    candidate.append(r)
        return candidate

    def generalize(self, candidate, idx):
        # drop literals from candidate as long as candidate is inductive relative to frame[idx]
        checked = []
        removed = []
        self._logger.debug("trying to generalize cube of size {} at index {}".format(len(candidate), idx))
        self._logger.spam(self.concretize(candidate))
        if len(candidate) > 1:
            for i in range(len(candidate)):
                if candidate[i] not in checked:
                    tmp = candidate[:i] + candidate[i+1:]
                    self._logger.spam("trying to drop {}".format(self.concretize([candidate[i]])[0]))
                    if self.is_initial(tmp) == z3.sat or not self.check_rel_ind(idx, tmp):
                        checked.append(candidate[i])
                    else:
                        removed.append(candidate[i])
                    if self.is_initial(tmp) in [z3.unsat, z3.unknown]:
                        self._solver.pop()
        candidate = [c for c in candidate if c not in removed]
        return candidate

    def push(self, candidate, idx):
        # find highest idx for which candidate is inductive relative to frame[idx]
        k = self.get_depth()
        while idx < k-1:
            if self.check_rel_ind(idx+1, candidate):
                candidate = self.get_candidate(candidate)
                idx += 1
            else:
                self._solver.pop()
                break
        return (candidate, idx)

    def add_blocked(self, candidate, idx):
        # add candidate to frames[0] ... frames[idx]
        for i in range(1, idx+1):
            frame = []
            for j in range(len(self._frames[i])):
                if self.subsumes(candidate, self._frames[i][j]):
                    self._stats['subsumed_cubes'] += 1
                else:
                    frame.append(self._frames[i][j])
            self._frames[i] = frame

        self._frames[idx].append(candidate)
        clauses = z3.Not(self._frameLabels[idx])
        for clause in candidate:
            clauses = z3.Or(clauses, self._converter.convert(Not(clause)))
        self._solver.add(clauses)
        self._stats['added_cubes'] += 1
        self._logger.debug("adding cube of size {} at level {}".format(len(candidate), idx))
        self._logger.spam(self.concretize(candidate))

    def subsumes(self, candidate, clauses):
        # check whether candidate subsumes clauses
        if len(candidate) <= len(clauses):
            if set(candidate).issubset(clauses):
                return True
        return False

    def add_frame(self):
        # add new frame and label
        if self.get_depth():
            out = "Depth: {}   Frame sizes:".format(self.get_depth())
            for frame in self._frames[1:]:
                out = "{} {}".format(out, len(frame))
            self._logger.notice("{}   Adding new frame".format(out))
        self._frames.append([])
        self._frameLabels.append(z3.FreshBool())

    def propagate(self):
        # forward propagation of cubes, checks if fixpoint is found
        start_time = time.time()
        k = self.get_depth()
        for i in range(1, k):
            add_clauses = []
            for j in self._frames[i]:
                self._logger.debug("trying to propagate cube of size {} from {} to {}".format(len(j), i, i+1))
                self._logger.spam(self.concretize(j))
                if self.check_rel_ind(i+1, j):
                    add_clauses.append(self.get_candidate(j))
                else:
                    self._solver.pop()
            for clause in add_clauses:
                self.add_blocked(clause, i+1)
            if len(self._frames[i]) == 0:
                self._logger.verbose("fixpoint found at frame {}".format(i))
                if self._options['witness']:
                    invariant = []
                    for j in range(i+1, len(self._frames)):
                        for k in self._frames[j]:
                            invariant.append([Not(clause) for clause in self.concretize(k)])
                    print("Invariant:")
                    for idx in range(len(invariant)):
                        print("Clause: {}".format(idx))
                        print("( or")
                        for clause in invariant[idx]:
                            print("  ", end=" ")
                            print(clause)
                        print(")")
                self._stats['propagate_time'] += time.time() - start_time
                return True
        self._stats['propagate_time'] += time.time() - start_time
        return False

    def refine_abstraction(self, cex):
        # check if cex is spurious and if yes, refine using interpolants
        self._stats['num_refinements'] += 1
        self._logger.notice("trying to refine counterexample of length {}".format(len(cex)))
        start_time = time.time()
        if len(self._preds) > 0:
            cex = [self.concretize(c) for c in cex]

        if self._refiner.refine(cex):
            preds = self._refiner.get_predicates(cex)
            preds = [pred for pred in preds if preds not in self._preds]
            for pred in preds:
                self.add_predicate(pred)
            if len(preds) == 0:
                self._logger.notice("refinement failed: no new predicates found")
                self._stats['refinement_time'] += time.time() - start_time
                return False
            self._logger.notice("refinement added: {} new predicates found".format(len(preds)))
            self._stats['refinement_time'] += time.time() - start_time
            return True
        else:
            self._cex = self._refiner.get_counterexample(cex)
            self._stats['refinement_time'] += time.time() - start_time
            return False

    def solve(self, assumptions):
        self._stats["solve_calls"] += 1
        # assumptions = [self._converter.convert(a) for a in assumptions]
        start_time = time.time()
        result = self._solver.check(assumptions)
        self._stats["solve_time"] += time.time() - start_time
        return result

    def get_logger_verbosity(self, verbosity):
        if verbosity >= 4:
            return logging.SPAM
        elif verbosity >= 3:
            return logging.DEBUG
        elif verbosity >= 2:
            return logging.VERBOSE
        elif verbosity >= 1:
            return logging.NOTICE
        elif verbosity == 0:
            return logging.WARNING

    def print_stats(self):
        print("\nDepth: {}".format(self.get_depth()))
        print("Number of predicates: {}".format(len(self._preds)))
        print("Number of refinements: {}".format(self._stats["num_refinements"]))
        print("Number of solve calls: {}".format(self._stats["solve_calls"]))
        print("Number of added cubes: {}".format(self._stats["added_cubes"]))
        print("Number of subsumed cubes: {}".format(self._stats["subsumed_cubes"]))
        print("Time taken by solve calls: {} seconds".format(round(self._stats["solve_time"], 5)))
        print("Time taken by blocking phase: {} seconds".format(round(self._stats["rec_block_time"], 5)))
        print("Time taken by propagation phase: {} seconds".format(round(self._stats["propagate_time"], 5)))
        print("Time taken to refine counterexamples: {} seconds".format(round(self._stats["refinement_time"], 5)))
        print("Total time taken: {} seconds\n".format(round(self._stats["total_time"], 5)))