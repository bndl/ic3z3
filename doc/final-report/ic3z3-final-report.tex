\documentclass[runningheads]{../proposal/llncs}

\usepackage{xcolor}
\usepackage{hyperref}

% display URLs in blue roman font according to Springer's eBook style
\renewcommand\UrlFont{\color{blue}\rmfamily}

%% \usepackage{unicode-math}
\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{mathtools}

\usepackage{listings}
\lstset{showstringspaces=false,basicstyle=\fontsize{9}{10}\selectfont\ttfamily,
numberstyle=\scriptsize,commentstyle=\color{darkgray}\itshape,escapeinside={(*}{*)}}


\begin{document}

\title{IC3z3: A Z3-based IC3 Implementation}

\author{Amin Bandali\inst{1} \and
  Khadija Tariq\inst{1}}

\authorrunning{A. Bandali and K. Tariq}

\institute{University of Waterloo, Waterloo ON N2L 3G1, Canada
  \email{\{abandali,k2tariq\}@uwaterloo.ca}}

\maketitle


\begin{abstract}
  IC3 is a state of the art SAT-based model checking algorithm without
  unrolling the transition relation.  IC3 and its variants are often
  implemented using SAT solvers, such as MiniSat.  In this work, we
  take on implementing IC3 using the Z3 SMT solver.  Z3 is a high
  performance and feature rich prover by the Research in Software
  Engineering (RiSE) group at Microsoft Research.  SAT solvers lend
  themselves nicely to hardware verification due to digital (Boolean)
  nature of circuits; whereas for practical software verification,
  various theories are often required; and this is where SMT solvers
  shine.  Not only it would be interesting to compare the performance
  of a SAT-based version of IC3 with an SMT-based version, but
  implementing an SMT-based version would lay the groundwork for
  future work of adding support for software verification.
% \keywords{IC3 \and Z3 \and SAT \and SMT.}
\end{abstract}


\section{Introduction}

Before IC3, state of the art model checking algorithms unrolled the
transition relation of the transition system, and presented the solver
with large formulas.  Then, Aaron R. Bradley introduced the IC3
algorithm, where IC3 stands for \textbf{I}ncremental
\textbf{C}onstruction of \textbf{I}nductive \textbf{C}lauses for
\textbf{I}ndubitable \textbf{C}orrectness.  IC3 is a symbolic model
checking algorithm that takes a conflict-directed approach of
incrementally generating and refining an inductive invariant that
strengthens the desired property, or presents a counterexample if it
fails to do so.  Originally used in hardware model checking with SAT
solvers, various improvements and
extensions~\cite{En2011EfficientIO,Ivrii:2015:PT:2893529.2893545} to
IC3 have been proposed, including extending IC3 to the general
Satisfiability Modulo Theories (SMT) case, as
in~\cite{10.1007/978-3-642-31424-7_23,10.1007/978-3-642-31612-8_13,10.1007/978-3-642-54862-8_4}.
Most methods proposed for generalizing IC3 from SAT to SMT rely on
techniques like quantifier elimination which become a performance
bottleneck, or on ad-hoc extensions to the algorithm such as
theory-specific generalization of clauses which are often not reusable
for other theories.  In~\cite{10.1007/978-3-642-54862-8_4} however,
Cimatti et al. proposed a novel approach for lifting IC3 to SMT using
Implicit Predicate Abstraction~\cite{10.1007/978-3-642-05089-3_7} that
avoids these pitfalls.  Their implementation, IC3ia, is written in C++
and uses the MathSAT5 SMT solver.  In this project, we take on the
task of re-implementing IC3ia and use
Z3~\cite{10.1007/978-3-540-78800-3_24} from Microsoft Research as our
SMT solver.

In this report we present background and notation in
Section~\ref{sec:bgn}---diving into (implicit) predicate abstraction in
\ref{sec:pa} and \ref{sec:ipa}, describe IC3z3 with IA in
Section~\ref{sec:ic3z3ia}, implementation and challenges in
Section~\ref{sec:impl}, experiments and our results in
Section~\ref{sec:exp}, related and future work in
Sections~\ref{sec:relwork} and \ref{sec:futwork}, and finally our
concluding remarks in Section~\ref{sec:conclusion}.


\section{Background and Notation}
\label{sec:bgn}

IC3z3, like IC3ia and the original IC3 by Bradley, operates in first
order logic.  We will denote logical formulas with the Greek letters
$\varphi$ (phi) and $\psi$ (psi), and letters like $F$, $T$, and $P$.  Further,
we denote variables with $x$ and $y$, and sets of variables with $X$,
$Y$, $\overline{X}$, and $\widehat{X}$.  We use Boolean variables to
refer to predicates with arity zero, theory variables to refer to
uninterpreted functions of arity zero, and we assume quantifier-free
variables unless explicitly noted otherwise.  In first order logic a
literal is an atomic formula or its negation, a clause is a
disjunction of literals, and a cube a conjunction of literals.  It
follows that the negation of a clause is a cube, and the negation of a
cube a clause.  Conjunctive Normal Form (CNF) refers to formulas that
are a conjunction of clauses, and Disjunctive Normal Form (DNF) to a
disjunction of cubes.  Given a formula $\varphi$ and a set of variables
$X_1,\ldots,X_n$, we use $\varphi(X_1,\ldots,X_n)$ to mean all the variables occurring
in $\varphi$ are elements of the union of the set of variables, $\bigcup_{i \in
  1..n}X_i$.  For every variable $x$, we will assume that there exist
corresponding primed ($x'$), overlined ($\overline{x}$), and hatted
($\hat{x}$) versions of $x$.  Thus, for a set $X$ of variables, we may
obtain the sets $X' = \{x' \mid x \in X\}$, $\overline{X} = \{\overline{x}
\mid x \in X\}$, and $\widehat{X} = \{\hat{x} \mid x \in X\}$.

\subsection{Transition System}

A transition system $S = \big<I, X, \mathit{Init}, \mathit{Tr}\big>$
is a tuple with $I$ being the set of input variables, $X$ the set of
states, $\mathit{Init}$ the set of initial states, and $\mathit{Tr}$
the transition relation.  A state of the transition system $S$ is an
assignment to its variables, and a path in $S$ is a finite sequence of
states $s_0,s_1,\ldots,s_k$ such that $s_0 \vDash \mathit{Init}$ and $\forall i \cdot 0 \leq
i \leq k, (s_i, s'_{i+1}) \vDash Tr$.  For a formula $P(X)$, the verification
problem $S \vDash P$ corresponds to checking whether for all paths of $S$,
each state entails the formula $P(X)$.  In other words, $\forall
s=s_0,s_1,\ldots,s_k, 0 \leq i \leq k, s_i \vDash P$.  The dual of the verification
problem is the reachability problem, where we try to find a path in
$S$ that violates $P$ (i.e. $s_k \vDash \neg{P}$ is a bad state).  Finally, a
formula $P$ is an inductive invariant of a transition system $S$ if
and only if (1) $\mathit{Init} \vDash P(X)$, and (2) $P(X) \wedge \mathit{Tr} \vDash
P(X')$.  Also, $P$ is said to be inductive relative to another formula
$\phi$ if $\phi(X)$ is a conjunct in the antecedent of condition (2) above.

\subsection{IC3 with SMT}

Bradley's original IC3~\cite{10.1007/978-3-642-18275-4_7} used a SAT
solver, and was used for verification of finite-state systems with
Boolean state variables and formulas in propositional logic, in areas
like hardware verification.  For instance, Bradley's own
IC3ref~\cite{ic3ref} reference implementation uses the MiniSat solver
and handles input in the AIGER And-Inverter Graph (AIG) format.  Being
efficient and a substantial improvement to its predecessors, IC3 has
become the focus of much research in the verification community in
both academia and industry, and has since been extended to the more
general SMT case, as described
in~\cite{10.1007/978-3-642-31424-7_23,10.1007/978-3-642-31612-8_13} as
well as~\cite{10.1007/978-3-642-54862-8_4}, in which Cimatti et
al. describe generalization of IC3 to SMT using Implicit Predicate
Abstraction to generalize IC3 to infinite-state transition systems
over a theory.

One of the main differences between the SAT and SMT cases of IC3 is
how the to-be-blocked or to-be-generalized sets of states are
created. When trying to block a pair $(s,i)$ in the blocking phase, if
the relative inductivity formula $F \wedge c \wedge Tr \wedge \neg{c'}$ is sat then a
new pair $(p,i-1)$ has to be generated where $p$ is a cube in the
pre-image of $s$ with respect to $Tr$.  In the propositional (SAT)
case, we can easily obtain $p$ from the model of the above formula
generated by the solver by dropping its primed variables.  But doing
so in the SMT case in general is not possible.  The authors
of~\cite{10.1007/978-3-642-31424-7_23} suggest computing $p$ by
existentially quantifying over and LRA-under-approximating the above
formula and eliminating the existential again.  This can be
computationally very expensive, and we will thus turn to implicit
predicate abstraction to forego this.

\subsection{Predicate Abstraction}
\label{sec:pa}

IC3z3, like IC3ia, uses predicate abstraction to reduce the search
space size.  Let $\widehat{S}$ be an abstraction of $S$.  If a set of
states is reachable in the concrete $S$, it will also be reachable in
the abstract $\widehat{S}$.  Therefore, by contrapositive, if the set
is not reachable in $\widehat{S}$ then it will not be reachable in $S$
either.

Given a transition system $S$, select a set $\mathbb{P}$ of predicates
where each $p \in \mathbb{P}$ is a formula over variables $X$
characterizing relevant facts of the system.  Then, for every
predicate $p \in \mathbb{P}$, we introduce new abstract variable $x_p$
and let $X_\mathbb{P} = \{x_p\}_{p \in \mathbb{P}}$.  We then define the
abstraction relation $H_\mathbb{P}$ as $H_{\mathbb{P}}(X,X_\mathbb{P})
\coloneqq \bigwedge_{p \in \mathbb{P}}x_p \leftrightarrow p(X)$.  For any concrete formula
$\phi(X)$, we obtain the abstract $\widehat{\phi}_\mathbb{P}$ by
existentially quantifying over the variables $X$.  For instance,
$\widehat{\phi}_\mathbb{P} = \exists X.(\phi(X) \wedge
H_{\mathbb{P}}(X,X_\mathbb{P}))$.  Finaly, we obtain the abstract
transition system $\widehat{S_\mathbb{P}} =
\big<X_\mathbb{P},\widehat{I_\mathbb{P}},\widehat{T_\mathbb{P}}\big>$
by abstracting over the initial and transition conditions.  Because
most model checkers expect quantifier-free formulas, we have to
eliminate the quantifiers of $\widehat{S_\mathbb{P}}$, which as
previously mentioned will be a performance bottleneck.

\subsection{Implicit Predicate Abstraction}
\label{sec:ipa}

To avoid having to perform quantifier elimination as mentioned
in~\ref{sec:pa}, we will use Implicit Predicate Abstraction.  The idea
is to embed the definition of predicate abstraction in the path
encoding to mimic IC3 working on abstract state space but use implicit
abstraction to avoid having to eliminate quantifiers when computing
the abstract transition relation.

We will now work towards formulating a BMC (Bounded Model Checking)
problem for the abstract space.  Firstly, instead of imposing a
strictly contiguous sequence of transitions, we will allow
disconnected transitions but force the gap between every pair of
transitions to fall in the same abstract state.  This is formalized by
the formula $EQ_\mathbb{P}(X,\overline{X}) \coloneqq \bigwedge_{p \in
  \mathbb{P}}p(X) \leftrightarrow p(\overline{X})$.  Next, we define a formula
$\widehat{Path}_{k, \mathbb{P}}$ to be satisfiable if and only if
there exists a path of $k$ steps over the predicates $\mathbb{P}$ in
the abstract space.  That is, $\widehat{Path}_{k, \mathbb{P}}
\coloneqq \bigwedge_{1 \leq h < k}(T(\overline{X}^{h-1}, X^h) \wedge
EQ_\mathbb{P}(X^h, \overline{X}^h)) \wedge T(\overline{X}^{k-1}, X^k)$.
Now the only remaining step is to add the abstract initial state,
transition relation, and bad state formula to define
$\mathrm{BMC}^k_\mathbb{P} \coloneqq I(X^0) \wedge
EQ_\mathbb{P}(X^0,\overline{X}^0) \wedge \widehat{Path}_{k, \mathbb{P}} \wedge
EQ_\mathbb{P}(X^k,\overline{X}^k) \wedge \neg{P}(\overline{X}^k)$.
$\mathrm{BMC}^k_\mathbb{P}$ is satisfiable if and only if there exists
a path of length $k$ over predicates $\mathbb{P}$ in the abstract
space which ends in a bad state.


\section{IC3z3 with Implicit Abstraction}
\label{sec:ic3z3ia}

\subsection{Goal}

Given a finite-state transition system $S = \big<I, X, \mathit{Init},
\mathit{Tr}\big>$ and a safety property $P(X)$, the IC3 algorithm
incrementally refines and extends a sequence of formulas $F_0, F_1, \ldots,
F_k$ where each $F_i$ is an over-approximation of the sets of states
reachable from $\mathit{Init}$ in at most $i$ steps.  The algorithm
either returns an inductive invariant or a counterexample $s_0, \ldots,
s_k$ where $s_k \nvDash P$.  The formulas all satisfy the following
properties:
\begin{enumerate}
\item $\mathit{Init} \Rightarrow F_0$
\item $F_i \Rightarrow F'_{i+1} \text{ for } 0 \leq i < k$
\item $F_i \wedge \mathit{Tr} \Rightarrow F'_{i+1} \text{ for } 0 \leq i < k$
\item $F_i \Rightarrow P \text{ for } 0 \leq i \leq k$
\end{enumerate}

\subsection{Algorithm}

The pseudo-code of the IC3z3 algorithm can be seen in~\ref{lst:main}.
The algorithm starts by extracting predicates from $\mathit{Init}$ and
$P$ and storing them separately so they can be used later to generate
new clauses.  After this, the algorithm checks for the satisfiability
of $\mathit{Init} \wedge \neg{P}$ to check for a 0-step counterexample.  If
the formula is satisfiable, a counterexample is found.  If it is
unsatisfiable, $F_0$ is initialized to $\mathit{Init}$ and the main
loop is executed.  The main loop consists of two main phases: the
blocking phase and the propagation phase.

In the \textbf{blocking phase}, the generalized predecessor of the bad
state is found and blocked in the appropriate frames until the
property holds.  If it is found that the bad state is reachable from
$\mathit{Init}$, the abstract counterexample is simulated in concrete
space to check if it is spurious or real.  If the counterexample is
spurious, refinement is applied to learn new predicates using the
existing predicates.  If the counterexample is real, the program
returns false; i.e. the program is declared unsafe.  The pseudo-code
for the blocking phase can be seen in~\ref{lst:rec}.  The function
uses an iterative approach by using a priority queue to store the
proof obligation pairs and blocking them one at a time until the queue
is empty.  When trying to block $s$ in $i$, the function checks if
$\neg{s}$ is inductive relative to the previous frame.  If it is, then
the generalized predecessor of $\neg{s}$ is added to the queue with index
$i-1$.  Otherwise, $\neg{s}$ is generalized and added to $i$-th
frame.

The \textbf{propagation phase} extends the frame sequence by one and
adds any clause $c \notin F_{i+1}$ where $F_i \wedge \mathit{Tr} \Rightarrow c'$ to
$F_{i+1}$.  If $F_i = F_{i+1}$ is discovered, we can return true and
conclude that the program is safe with invariant $F_i$.

%% IC3+IA listing
\input{alg-ic3+ia}

\subsection{Refinement}

During the blocking phase, if the \texttt{rec\_block} function
(see~\ref{lst:rec}) finds a proof obligation pair ($s_0$, $0$), it
will return false.  The algorithm first extracts the abstract
counterexample $\pi$ from the priority queue where $\pi = (s_0,
0);\ldots;(s_k,k)$ and simulates it in the concrete space (via BMC) to
check if it is spurious or real.  The counterexample is real if
$\mathit{I(X^0)\wedge \bigwedge_{i<k}T(X^i,X^{i+1}) \wedge P(X^k)\wedge \bigwedge_{i \leq k}s_k(X^k)}$
is satisfiable.  In case of unsatisfiability, the refiner looks for
new predicates to remove the spurious counterexample.  This can be
done in a number of different ways, e.g. using unsat core extraction
or using interpolants.

\enlargethispage{3\baselineskip}

%% rec_block listing
\input{alg-rec_block}


\section{Implementation}
\label{sec:impl}

\subsection{Implementation details}

Our implementation follows the same structure as the one mentioned
in~\cite{10.1007/978-3-642-31424-7_23}.  The only difference between
the implementation of IC3ia~\cite{ic3ia} and IC3z3~\cite{ic3z3} is
that IC3z3 is written in Python and uses the Z3 solver, whereas IC3ia
is written in C++ and uses the MathSAT5 solver.  There are several
reasons why we decided to use Z3.  Firstly, Z3 is a performant prover
with a nice feature set.  Thus, using Z3 as the solver opens the door
to future work such as extending the IC3 implementation with support
for theories, to get IC3 Modulo Theories.  Further, like MathSAT5, Z3
supports solving formulas over LRA and BV.  Z3 can be used
incrementally, so only one instance of the solver is needed for all
calls; which helps improve efficiency.

For now, the implementation supports Python 3 only and the algorithm
accepts input files in the VMT format only.  The code makes use of
pySMT---a library for manipulating and solving SMT formulas---to read the
VMT program and extract formulas with their respective annotations.
PySMT also supports theories including BV, LIA, and LRA, and provides
support for various solvers including MathSAT and Z3 for easier use.
The library also allows converting a formula from pySMT to Z3.  IC3z3
uses this converter and the Z3 SMT solver to check for each condition.
It also uses the MathSAT solver support available under pySMT to find
interpolants to extract new predicates during refinement.

\subsection{Optimizations}

Firstly, the algorithm handles Boolean variables separately.  In order
to improve efficiency, abstraction is not applied to Boolean variables
to make it easier to apply refinement later.  The code uses the model
of Z3 solver to get $c$ in case it finds an intersection between
$i$-th frame and $\neg{P}$.  In order to generalize $c$, the algorithm
makes use of the unsat core of Z3.  Before adding $\neg{c}$ to the
appropriate frame, the algorithm first looks for a stronger clause
$\neg{d}$ which satisfies the property $\neg{d} \wedge F[i-1] \wedge Tr \wedge d$.  The
algorithm does this by dropping literals from $c$ and checking if the
property still holds.  The algorithm also saves space and effort by
removing subsumed clauses from frames.  Another optimization done in
IC3z3 is that the predicates obtained by interpolants are minimized
before they are added to the system.  This helps limit the number of
predicates and improve efficiency.

\subsection{Challenges}

During the course of the project, we face a number of challenges which
we have listed below:

Firstly, the algorithm needs interpolants for the refinement step, but
Z3's interpolation facilities didn't work completely in previous
versions, and the interpolation API has been completely removed in the
more recent versions.  As a temporary solution, we opted to use
MathSAT for getting interpolants, with the longer term goal of
eliminating dependency on MathSAT as future work.

Secondly, after debugging effort we discovered that the unsat
assumptions facility of pySMT is in fact quite buggy, and we thus had
to convert clauses from pySMT to Z3 structures and resorted to using
the Z3 Python API directly before solving.  This raises efficiency
concerns, taking into account the time taken to convert between the
two representations, Z3's relatively slow Python API, and also IC3z3's
use of a mixture of pySMT, Z3, and MathSAT.

Lastly, we found that the latest available release of pySMT is
incompatible with the latest Python 3.7, and one of the authors whose
GNU/Linux installation ships Python 3.7 had to try and work around the
issue by manually building and installing Python 3.6, which turned out
to be no small task.


\section{Experimental Evaluation}
\label{sec:exp}

For testing our implementation, we will be using VMT models, the same
data used by the Griggio paper~\cite{10.1007/978-3-642-31424-7_23},
and compare the results for selected benchmarks.  We took a total of
100 benchmarks from different sources, including sources using
bit-vector theory (QF\_BV), concurrent program using linear-integer
arithmetic (QF\_LIA), and Lustre programs, taken from the Kind model
checker benchmark suite, also using QF\_LIA.  The selected benchmarks
used to generate the result below are also available
at~\cite{ic3z3}.

Of these selected benchmarks, 86\% of the programs were safe and 14\%
were unsafe.  \textbf{Table~\ref{table:tbl1}} below shows the relevant
statistics from executing selected benchmarks using the IC3z3
algorithm.  \textbf{Table~\ref{table:tbl2}} shows the runtime data
from executing these same benchmarks.

Of these selected benchmarks, 86\% of the programs were safe and 14\%
were unsafe.  \textbf{Table~\ref{table:tbl1}} below shows the relevant
statistics from executing selected benchmarks using the IC3z3
algorithm.  The table indicates the benchmark theory, the trace length
(\textbf{Depth}) which signifies the number of major iterations
executed by the algorithm, the total number of predicates (\textbf{\#
  predicates}) including the initial ones and those found through
refining later on, the number of times an abstract counterexample was
found to be spurious and refined (\textbf{\# refinements}), and
lastly, it shows the total number of solve calls executed by the SMT
solver.  \textbf{Table~\ref{table:tbl2}} shows the runtime data from
executing these same benchmarks.  The table includes the time taken by
the SMT solver to solve all checks, time taken by the blocking phase
and the propagation phase, the time taken by the refiner and finally,
the total time taken to check the safety of that program.  Analyzing
these results, we can see that the efficiency of the algorithm is
\emph{not} significantly reduced as the number of predicates and
refinements increase.  It can also be seen that thousands of SMT
queries can be executed in seconds.  For instance, on the
\textbf{s3\_clnt\_4.vmt} file, the algorithm takes 6.4631 seconds to
execute 1646 solve calls.

The runtime data for all 100 benchmarks is used to plot a few graphs
as seen below. The red circles represent safe programs whereas, the
blue rectangles represent unsafe programs.
\textbf{Figure~\ref{Fig:img1}} plots the total number of predicates
found by the system against the time taken by the algorithm to refine
spurious counterexamples and extract new predicates using the previous
predicates.  The graph shows that as the number of predicates
increase, the time taken by refinement also increases.
\textbf{Figure~\ref{Fig:img2}} plots a graph showing a similar trend
with total number of predicates against the total time taken by the
algorithm.  \textbf{Figure~\ref{Fig:img3}} shows the relationship
between depth and the total time taken by the system.  We can see that
the total time in seconds increases as the depth increases, as
expected.  \textbf{Figure~\ref{Fig:img4}} plots the results obtained
from running the same benchmarks on IC3ia~\cite{ic3ia} and on
IC3z3~\cite{ic3z3}.  We hypothesize that a number of different factors
could influence the running time of IC3z3, making it seem longer to
run, compared to IC3ia.  Namely, the order of dropping literals can
vary, and the models obtained from each of the SMT solvers can be
different too.  Further, in the currnet implementation of IC3z3 we use
pySMT, Z3, and MathSAT; whereas IC3ia uses only MathSAT for
everything.  Further, IC3ia is written in C++ and uses MathSAT's C
API; whereas IC3z3 is written in Python and uses Z3's Python API,
which is considerably slower compared to its C API, due to the various
safety checks and harnesses built into its Python API.  Interestingly
enough, IC3z3 does outperform IC3ia in some of the benchmarks, which
goes to show the efficiency of Z3 compared to MathSAT despite all the
other factors slowing IC3z3 down when compared to IC3ia.

\input{tbl1}

\input{tbl2}

\begin{figure}[htbp]
    \begin{minipage}{0.6\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{preds_refine.png}
        \caption{Number of predicates vs. Refinement time}\label{Fig:img1}
    \end{minipage}\hfill
    \begin{minipage}{0.6\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{preds_ttime.png}
        \caption{Number of predicates vs. Total time}\label{Fig:img2}
    \end{minipage}\hfill
\end{figure}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.5\linewidth]{depth_ttime.png}
    \caption{Depth vs. Total time}\label{Fig:img3}
\end{figure}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.5\linewidth]{compare.png}
    \caption{Time taken by IC3z3 vs. Time taken by IC3ia}\label{Fig:img4}
\end{figure}

\section{Related Work}
\label{sec:relwork}

At the time, state of the art SAT-based model checkers would unroll
the transition relation of the system, presenting the solver with
large formulas.  Then Aaron R. Bradley introduced
IC3~\cite{10.1007/978-3-642-18275-4_7,10.1007/978-3-642-31612-8_1}, a
new SAT-based symbolic model checking algorithm that would, instead of
unrolling the transition relation, incrementally generate and refine
an inductive invariant strengthening the property, or present a
counterexample.  Over the course of years, IC3 has proven to be one of
the best SAT-based model checking algorithms, inspiring different
variations~\cite{gurfinkel_ssft15} by researchers across the field.
The original IC3 implementation used the MiniSat SAT solver.
A. Griggio and A. Cimatti went on to extend the SAT-based
implementation of IC3 to SMT~\cite{10.1007/978-3-642-31424-7_23}.  In
order to do that, they modified the blocking phase of the algorithm to
work with the MathSAT5 SMT solver by using the theory of Linear
Rational Arithmetic (LRA) to compute the under-approximation of
pre-image of the blocking state.  In a later
work~\cite{10.1007/978-3-642-54862-8_4} with S. Mover and S. Tonetta,
Cimatti and Griggio proposed using Implicit Predicate Abstraction to
generalize IC3 to infinite-state transition systems over a theory.  In
this novel approach, reasoning about the theory is done within the SMT
solver, with IC3 operating at the surface level in the abstract state
space and finding inductive clauses over the abstraction predicates.
This approach eliminates the need for ad-hoc extensions for supporting
a wide range of theories, as well as performance improvements due to
use of abstraction.


\section{Future Work}
\label{sec:futwork}

For future work, we would like to
\begin{itemize}
\item swap out MathSAT in favour of Z3's spacer engine for CHC-based
interpolation,
\item use Z3 constructs directly to avoid the overhead of going back
  and forth between pySMT and Z3,
\item support all Python versions including the latest 3.7 and the old
  2.x,
\item run IC3z3 on the rest of the examples and benchmarks shipped
  with IC3ia and more theories such as QF\_LRA, and
\item replace our use of Z3 Python API with directly using Z3's C API
  using the C Foreign Function Interface for Python (CFFI).  A Haskell
  implementation using Z3's C API via the Haskell FFI is also planned,
  as an exercise of comparing the performance and also the required
  implementation effort across the two languages.
\end{itemize}


\pagebreak
\section{Conclusion}
\label{sec:conclusion}

Doing a (re-)implementation of IC3 modulo theories with implicit
predicate abstraction by Cimatti et al., we observed the benefits of
implicit abstraction such as not having to implement ad-hoc
theory-specific extensions, and that the IC3+IA approach performs
quite well, with the solver solving thousands of dispatched solve
calls per second.  Further, we enjoyed the ease of use of the Z3 and
pySMT Python libraries.  We learned that, though convenient, the unsat
assumptions facilities of pySMT are buggy at present, and we would be
better off directly using the Z3 Python API.

Our results in Section~\ref{sec:exp} showed that as the number of
predicates increases, the refinement time increases as well; and that
an increase in depth results in increase in total time.  Our
experiments and results are also a testament to Z3's impressive
performance, since despite the multiple factors slowing our
implementation down as discussed in Section~\ref{sec:exp}, our IC3z3
implementation using Z3 still managed to outperform the original IC3ia
(which uses MathSAT) in some of the benchmarks.


\bibliographystyle{../proposal/splncs04}
\bibliography{refs}

\end{document}
