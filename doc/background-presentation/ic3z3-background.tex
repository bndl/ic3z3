% Created 2018-11-30 Fri 07:52
% Intended LaTeX compiler: pdflatex
\documentclass[presentation]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{color}
\usepackage{listings}
\usepackage{listings}
\usepackage{color}
\setbeamertemplate{itemize item}[circle]
\setbeamertemplate{itemize subitem}{-}
\setbeamertemplate{itemize subsubitem}[circle]
\setbeamercolor{itemize item}{fg=darkred}
\setbeamercolor{itemize subitem}{fg=darkred}
\setbeamercolor{itemize subsubitem}{fg=darkred}
\usepackage{unicode-math}
\usefonttheme[onlymath]{serif}
\titlegraphic{\hfill
\includegraphics[height=1.3cm]{./Waterloo_MATH_DCheriton_CompSci_cmyk}}
\newcommand\scalemath[2]{\scalebox{#1}{\mbox{\ensuremath{\displaystyle #2}}}}
\usetheme{default}
\usecolortheme{beaver}
\author{Khadija Tariq, Amin Bandali\thanks{\{k2tariq,abandali\}@uwaterloo.ca}}
\date{November 30, 2018}
\title{IC3z3: A Z3-based IC3 Implementation}
\subtitle{Background Presentation}
\hypersetup{
 pdfauthor={Khadija Tariq, Amin Bandali},
 pdftitle={IC3z3: A Z3-based IC3 Implementation},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.0.50 (Org mode 9.1.14)}, 
 pdflang={English}}
\begin{document}

\maketitle
\setbeamercovered{transparent}

\begin{frame}[label={sec:org2c59736}]{What is IC3?}
{
  \textbf{I}ncremental \textbf{C}onstruction of \textbf{I}nductive
  \textbf{C}lauses for \textbf{I}ndubitable \textbf{C}orrectness
}

\begin{itemize}
\item safety model-checking algorithm
\item incrementally generates clauses which represent reachability
information up to a certain depth
\item keep increasing depth until counterexample found or property is
proven
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org22cda0c}]{IC3 Structure}
Given a finite-state transition system \(S(X, \mathit{Init},
\mathit{Tr})\) and safety property \(P(X)\), incrementally refines and
extends a sequence of formulas \(F₀,F₁,…,F_k\) such that \medskip

\begin{itemize}
\item \(\mathit{Init} ⇒ F₀\)
\item \(F_i ⇒ F'_{i+1}\) for \(0 ≤ i < k\)
\item \(F_i ∧ \mathit{Tr} ⇒ F'_{i+1}\) for \(0 ≤ i < k\)
\item \(F_i ⇒ P\) for \(0 ≤ i ≤ k\)
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org28f44be}]{IC3 Algorithm}
Check for initial cases first: \hspace{2em} \(\mathit{Init}∧¬P\)
\hspace{2em} \(\mathit{Init}∧\mathit{Tr}'∧¬P\) \bigskip

Iterate over blocking phase or the propagation phase by checking for
\(F_k∧\mathit{Tr}⇒P'\) \bigskip

\alert{Blocking phase:} find generalized predecessor of bad state and block
it in appropriate until property holds \bigskip

\alert{Propagation phase:} extends the frame sequence by one and adds any
clause \(c∉F_{i+1}\) where \(F_i ∧ \mathit{Tr} ⇒ c'\) to \(F_{i+1}\)
\end{frame}

\begin{frame}[label={sec:orgb38e4e7}]{IC3 extended to SMT}
Originally, SAT solvers were used and IC3 was mainly used for hardware
verification \medskip

Input: AIGER format \bigskip

IC3 implemented with SMT solvers so can be used for software
verification \medskip

Input: SMT, VMT, C programs
\end{frame}

\begin{frame}[label={sec:orgbc8d85d}]{IC3 Modulo Theory with Implicit Predicate Abstraction}
\begin{itemize}[<+->]
\item Deal with infinite-state systems by expressing abstract transitions
without explicitly computing the system in the abstract space
\item Use Implicit Abstraction to get an abstract version of the relative
induction check
\item When finding abstract counterexample, simulate in the concrete
space.  If spurious, refine the abstraction by adding predicates to
weed out the counterexample
\end{itemize}
\end{frame}

\begin{frame}[label={sec:orga6da402}]{Predicate Abstraction}
Given TS \(S\), select a set \(ℙ\) of predicates where each \(p∈ℙ\) is a
formula over variables \(X\) characterizing relevant facts of system.
\pause \bigskip

\(∀p∈ℙ\), introduce new abstract variable \(x_p\) and let \(X_ℙ =
\{x_p\}_{p∈ℙ}\).\pause\smallskip\\
Define abstraction relation \(H_ℙ\) as \(H_ℙ(X,X_ℙ) ≔ ⋀_{p∈ℙ}x_p ↔
p(X)\).\pause\\
For any concrete formula \(ϕ(X)\), obtain the abstract version
\(\hat{ϕ}_ℙ\) by existentially quantifying the variables \(X\).  For
instance, \(\hat{ϕ}_ℙ = ∃X.(ϕ(X)∧H_ℙ(X,X_ℙ))\).\pause\bigskip

Obtain abstract TS \(\hat{S_ℙ} = ⟨X_ℙ,\hat{I_ℙ},\hat{T_ℙ}⟩\) by abstracting
over the initial and transition conditions.  Eliminate quantifiers of
\(\hat{S_ℙ}\).
\end{frame}

\begin{frame}[label={sec:org7be003b}]{Implicit Predicate Abstraction}
Embeds the definition of predicate abstraction in path encoding.

\begin{onlyenv}<1>
\[ EQ_ℙ(X,\overline{X}) ≔ ⋀_{p∈ℙ}p(X)↔p(\overline{X}) \]
\[ \widehat{Path}_{k, ℙ} ≔  ⋀_{1≤h<k}(T(\overline{X}^{h-1}, X^h)∧EQ_ℙ(X^h, \overline{X}^h))∧T(\overline{X}^{k-1}, X^k) \]
\[ \mathrm{BMC}^k_ℙ ≔ I(X^0) ∧  EQ_ℙ(X^0, \overline{X}^0) ∧ \widehat{Path}_{k, ℙ} ∧ EQ_ℙ(X^k, \overline{X}^k) ∧ ¬P(\overline{X}^k) \]
\end{onlyenv}
\begin{onlyenv}<2>
\[ \scalemath{0.8}{EQ_ℙ(X,\overline{X}) ≔ ⋀_{p∈ℙ}p(X)↔p(\overline{X})} \] \vspace{-1em}
\[ \scalemath{0.8}{\widehat{Path}_{k, ℙ} ≔  ⋀_{1≤h<k}(T(\overline{X}^{h-1}, X^h)∧EQ_ℙ(X^h, \overline{X}^h))∧T(\overline{X}^{k-1}, X^k)} \] \vspace{-1em}
\[ \scalemath{0.8}{\mathrm{BMC}^k_ℙ ≔ I(X^0) ∧  EQ_ℙ(X^0, \overline{X}^0) ∧ \widehat{Path}_{k, ℙ} ∧ EQ_ℙ(X^k, \overline{X}^k) ∧ ¬P(\overline{X}^k)} \]
\vspace{-1.75em}
\begin{figure}[htbp]
\centering
\includegraphics[width=0.45\linewidth]{./abstract-path.png}
\caption{Abstract path}
\end{figure}
\end{onlyenv}

\begin{onlyenv}<3>

The idea of IC3+IA is to mimic IC3 working on abstract state space
(defined by \(ℙ\)) but use IA to avoid having to eliminate quantifiers
to compute the abstract transition relation.  So, clauses, frames, and
cubes will be forced to have predicates in \(ℙ\) as atoms.

\end{onlyenv}
\end{frame}

\begin{frame}[label={sec:orgd940702}]{Simulation and refinement}
For set \(π = (s₀,0);...;(s_k,k)\) of cex's found in abstract space,

\begin{itemize}
\item simulate \(π\) in concrete space (via BMC)
\begin{itemize}
\item \(I(X^0)∧⋀_{i<k}T(X^i,X^{i+1})∧P(X^k)∧⋀_{i≤k}s_k(X^k)\) SAT?
\begin{itemize}
\item yes: satisfying assignment is concrete witness to \(S⊭P\)
\item no: \(π\) is spurious
\end{itemize}
\end{itemize}
\item if no concrete cex witness, \(\mathit{refine}(I,T,ℙ,π)\)
\begin{itemize}
\item \(\mathit{refine}\) is orthogonal to IC3+IA
\item e.g. using interpolation or unsat core extraction
\item only requirement: must remove spurious cex's \(π\)
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[label={sec:orgfa527ee},fragile]{IC3z3}
 \begin{itemize}
\item Python instead of C++
\item Z3 instead of MathSat
\item pySMT
\begin{itemize}
\item for parsing input (supports annotations)
\item for interacting with the solver
\end{itemize}
\item Input format: VMT
\begin{itemize}
\item extents SMT2 to represent symbolic transition systems
\item uses annotations to specify the TS components\\
e.g. \texttt{:next}, \texttt{:trans}, \texttt{:invar-property}
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org178b84a}]{Challenges}
Designing the data structures \bigskip

Interpolants needed for refinement \bigskip

Incremental solving using Z3 \bigskip

\appendix
\end{frame}
\begin{frame}[label={sec:orge9e3da0}]{References}
\nocite{*}
\apptocmd{\thebibliography}{\renewcommand{\sc}{}}{}{}
\bibliographystyle{acm}
\bibliography{refs}
\end{frame}

\begin{frame}[label={sec:org9f7bc2c}]{}
\begin{center}
{\large Questions?}
\end{center}
\end{frame}

\begin{frame}[label={sec:org8bf3c9f}]{Appendix (IC3+IA)}
\begin{figure}[htbp] \centering
\includegraphics[width=0.55\linewidth]{./ic3-ia.png}
\caption{IC3+IA high-level description, with changes from the Boolean
  case in red.  From~\cite{10.1007/978-3-642-54862-8_4}.}
\end{figure}
\end{frame}
\end{document}
