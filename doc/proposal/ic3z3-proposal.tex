\documentclass[runningheads]{llncs}

% \usepackage{graphicx}

\usepackage{xcolor}
\usepackage{hyperref}
% display URLs in blue roman font according to Springer's eBook style
\renewcommand\UrlFont{\color{blue}\rmfamily}

%% \usepackage{unicode-math}
\usepackage{amsmath}
\usepackage{amssymb}

\newcommand{\n}{\overline}  % not

\begin{document}

\title{\texttt{IC3z3}: A Z3-based IC3 Implementation}

\author{Amin Bandali\inst{1} \and
  Khadija Tariq\inst{1}}

\authorrunning{A. Bandali and K. Tariq}

\institute{University of Waterloo, Waterloo ON N2L 3G1, Canada
\email{\{abandali,k2tariq\}@uwaterloo.ca}}

\maketitle

\begin{abstract}
  IC3 is a state of the art SAT-based model checking algorithm without
  unrolling the transition relation.  IC3 and its variants are often
  implemented using SAT solvers, such as MiniSat.  In this work, we
  take on implementing IC3 using the Z3 SMT solver.  Z3 is a high
  performance and feature rich prover by the Research in Software
  Engineering (RiSE) group at Microsoft Research.  SAT solvers lend
  themselves nicely to hardware verification due to digital (Boolean)
  nature of circuits; whereas for practical software verification,
  various theories are often required; and this is where SMT solvers
  shine.  Not only it would be interesting to compare the performance
  of a SAT-based version of IC3 with an SMT-based version, but
  implementing an SMT-based version would lay the groundwork for
  future work of adding support for software verification.

% \keywords{IC3 \and Z3 \and SAT \and SMT.}
\end{abstract}


\section{Algorithm}

Given a finite-state transition system $S(I, X, \mathit{Init},
\mathit{Tr})$ ($I$ the set of input variables, $X$ the set of states,
$\mathit{Init}$ the set of initial states, and $\mathit{Tr}$ the
transition relation) and a safety property $P(X)$, the IC3 algorithm
incrementally refines and extends a sequence of formulas $F_0, F_1, \ldots,
F_k$ where each $F_i$ is an over-approximation of the sets of states
reachable from $\mathit{Init}$ in at most $i$ steps.  The algorithm
either returns an inductive invariant or a counterexample $s_0, \ldots,
s_k$ where $s_k \nvDash P$.  The formulas all satisfy the following
properties:

\begin{enumerate}
\item $I \Rightarrow F_0$
\item $F_i \Rightarrow F'_{i+1} \text{ for } 0 \leq i < k$
\item $F_i \wedge Tr \Rightarrow F'_{i+1} \text{ for } 0 \leq i < k$
\item $F_i \Rightarrow P \text{ for } 0 \leq i \leq k$
\end{enumerate}

The algorithm first checks for the satisfiability of $\mathit{Init} \wedge
\neg{P}$ and $\mathit{Init} \wedge Tr \wedge \neg{P}'$.  If at least one is
unsatisfiable, a counterexample is found.  If both are satisfied,
$F_0$ is initialized to $\mathit{Init}$ and $F_i$ to $P$ for all $i >
0$.  Then the algorithm iterates over the propagation phase or the
blocking phase depending on if $F_k \wedge Tr \Rightarrow P'$ or not, respectively.
The propagation phase extends the frame sequence by one and adds any
clause $c \notin F_{i+1}$ where $F_i \wedge Tr \Rightarrow c'$ to $F_{i+1}$.  If $F_i =
F_{i+1}$ is discovered, we can conclude that $P$ is an inductive
invariant.  In the blocking phase, the generalized predecessor of the
bad state is found and blocked in the appropriate frames until the
property holds.  If it is found that the bad state is reachable from
$\mathit{Init}$, a counterexample is returned.


\section{Literature Review}

At the time, state of the art SAT-based model checkers would unroll
the transition relation of the system, presenting the solver with
large formulas.  Then Aaron R. Bradley introduced
IC3~\cite{10.1007/978-3-642-18275-4_7,10.1007/978-3-642-31612-8_1}, a
new SAT-based symbolic model checking algorithm that would, instead of
unrolling the transition relation, incrementally generate and refine
an inductive invariant strengthening the property, or present a
counterexample.  Over the course of years, IC3 has proven to be one of
the best SAT-based model checking algorithms, inspiring different
variations~\cite{gurfinkel_ssft15} by researchers across the field.  The original IC3
implementation used the MiniSat SAT solver.  A. Griggio and A. Cimatti
went on to extend the SAT-based implementation of IC3 to
SMT~\cite{10.1007/978-3-642-31424-7_23}.  In order to do that, they
modified the blocking phase of the algorithm to work with the MathSAT5
SMT solver by using the theory of Linear Rational Arithmetic (LRA) to
compute the under-approximation of pre-image of the blocking state.
In a later work~\cite{10.1007/978-3-642-54862-8_4} with S. Mover and
S. Tonetta, Cimatti and Griggio proposed using Implicit Predicate
Abstraction to generalize IC3 to infinite-state transition systems
over a theory.  In this novel approach, reasoning about the theory is
done within the SMT solver, with IC3 operating at the surface level in
the abstract state space and finding inductive clauses over the
abstraction predicates.  This approach eliminates the need for ad-hoc
extensions for supporting a wide range of theories, as well as
performance improvements due to use of abstraction.


\section{Implementation}

We will be implementing the IC3 algorithm in the Python programming
language.  However, instead of using a SAT solver, we will use the Z3
SMT solver, for a number reasons.  Firstly, Z3 is a performant prover
with a nice feature set.  Thus, using Z3 as the solver opens the door
to future work such as extending the IC3 implementation with support
for theories, to get IC3 Modulo Theories.  Further, similar to
MathSAT5, Z3 supports solving formulas over LRA.

For testing our implementation, we will be using the models of
And-Inverter Graphs (AIGs) available for HWMCC'10~\cite{hwmcc10}, the
same data used by the original Bradley paper, and compare the results
for selected benchmarks.


\bibliographystyle{splncs04}
\bibliography{refs}

\end{document}
